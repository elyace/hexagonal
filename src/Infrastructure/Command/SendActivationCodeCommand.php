<?php

namespace App\Infrastructure\Command;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\AccountNotification\SendActivationCodeUseCase;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:send-activation',
    description: 'Send activation code to customer'
)]
class SendActivationCodeCommand extends Command
{

    public function __construct(
        private readonly CustomerRepositoryInterface $repository,
        private readonly CustomerActivationCodeHasherInterface $hasher,
        private readonly UseCaseFactoryInterface $useCaseFactory
    )
    {
        parent::__construct("app:send-activation");
    }

    protected function configure(): void
    {
        $this->addArgument('email', InputArgument::REQUIRED, 'Customer email');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getArgument('email');
        /** @var Customer|null $customer */
        $customer = $this->repository->findOneByEmail($email);
        if( null === $customer )
        {
            $output->writeln("Customer with email {$email} does not exist");

            return Command::INVALID;
        }
        $newCode = $this->generateCode();
        $output->writeln('Generating activation code ...');
        $customer->activationCode = new CustomerActivationCode($this->hasher->hashCode($newCode));
        $this->repository->save($customer);
        $output->writeln("New activation code is $newCode");
        $output->writeln('Sending activation code ...');
        $this->useCaseFactory->setUseCase(SendActivationCodeUseCase::class)
            ->execute(new ActivationCode(
                $newCode,
                $customer->id
            ));

        return Command::SUCCESS;
    }

    private function generateCode(): string
    {
        $code = '';
        for ($i = 0; $i < 6; $i++) {
            $code .= mt_rand(0, 9);
        }

        return $code;
    }
}