<?php

namespace App\Infrastructure\Controller\Admin;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Property\Data\Model\PropertyType;
use App\Infrastructure\Database\Adapter\Property\Property;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{

    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[Route('/{_locale}/admin', name: 'admin-dashboard',)]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(CustomerCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('PP Rent');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud($this->translator->trans('Customers'), 'fas fa-address-book', Customer::class);
        yield MenuItem::section($this->translator->trans('Property Management'));
        yield MenuItem::linkToCrud($this->translator->trans('Property'), 'fas fa-building', Property::class);
        yield MenuItem::linkToCrud($this->translator->trans('Property types'), 'fas fa-leaf', PropertyType::class);
    }
}
