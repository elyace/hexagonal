<?php

namespace App\Infrastructure\Controller\Admin;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Shared\Data\Constant\RoleConstant;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Data\ValueObject\Roles;
use App\Infrastructure\Symfony\Form\Type\CustomerIdType;
use App\Infrastructure\Symfony\Form\Type\CustomerNameType;
use App\Infrastructure\Symfony\Form\Type\EmailType;
use App\Infrastructure\Symfony\Form\Type\RolesType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[IsGranted(RoleConstant::ADMIN_ROLE)]
class CustomerCrudController extends AbstractCrudController
{

    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly PasswordHasherFactoryInterface $hasherFactory
    )
    {
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural( $this->translator->trans('Customers') )
            ->setEntityLabelInSingular($this->translator->trans('Customer'))
            ->setPageTitle('new', $this->translator->trans('Add new customer'))
            ->setPageTitle('edit', $this->translator->trans('Update'))
        ;
    }

    public static function getEntityFqcn(): string
    {
        return Customer::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->onlyOnIndex()
                ->setFormType(CustomerIdType::class)
            ,
            TextField::new('name', $this->translator->trans('Name'))
                ->setFormType(CustomerNameType::class)
            ,
            TextField::new('email')
                ->setFormType(EmailType::class)
                ->formatValue(function (Email $value){
                    return $value->getValue(); // because __toString hide some email parts
                })
            ,
            ArrayField::new('roles', 'Roles')
                ->setFormType(RolesType::class)
                ->formatValue(function (Roles $value){
                    $roles = [];
                    foreach ($value->getValue() as $role)
                    {
                        $roles[] =  $this->translator->trans(
                            strtolower(explode('_', $role)[1])
                        );
                    }

                    return implode(' | ', $roles);
                })
            ,
            BooleanField::new('isActive', 'Active')->onlyOnIndex(),
            TextField::new('password', 'Mot de passe')
                ->hideWhenUpdating()
                ->hideOnIndex()
                ->hideOnDetail()
                ->setFormType(PasswordType::class)
        ];
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $this->addPasswordHasher($formBuilder);

        return $formBuilder;
    }

    public function createEntity(string $entityFqcn): Customer
    {
        return new Customer(
            CustomerId::make(),
            new CustomerName(''),
            new Email(''),
            '',
            new Roles([RoleConstant::CUSTOMER_ROLE])
        );
    }

    /**
     * @param FormBuilderInterface $formBuilder
     * @return void
     */
    private function addPasswordHasher(FormBuilderInterface $formBuilder): void
    {
        $hasher = $this->hasherFactory->getPasswordHasher('customer');
        $formBuilder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (PreSubmitEvent $event) use ($hasher) {
                $data = $event->getData();
                if('' !== $data['password'] && null !== $data['password']){
                    $hash = $hasher->hash($data['password']);
                    $event->setData([...$data, 'password' => $hash]);
                }
            }
        );
    }
}
