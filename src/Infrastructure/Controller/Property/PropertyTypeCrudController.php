<?php

namespace App\Infrastructure\Controller\Property;

use App\Domain\Property\Data\Model\PropertyType;
use App\Domain\Property\Data\ValueObject\PropertyTypeId;
use App\Domain\Shared\Data\Constant\RoleConstant;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[IsGranted(RoleConstant::SOFT_PROPERTY_MANAGER_ROLE)]
class PropertyTypeCrudController extends AbstractCrudController
{

    public function __construct(private readonly TranslatorInterface $trans)
    {
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', $this->trans->trans('Property types'))
            ->setPageTitle('new', $this->trans->trans('Add property type'))
            ->setPageTitle('edit', $this->trans->trans('Update'))
            ->setEntityLabelInSingular($this->trans->trans('Property type'))
            ->setEntityLabelInPlural($this->trans->trans('Property types'))
        ;
    }

    public static function getEntityFqcn(): string
    {
        return PropertyType::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('label', $this->trans->trans('Label')),
        ];
    }

    public function createEntity(string $entityFqcn): PropertyType
    {
        return new PropertyType(
            PropertyTypeId::make(),
            ''
        );
    }
}
