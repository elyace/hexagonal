<?php

namespace App\Infrastructure\Controller\Property;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Property\Data\Model\PropertyType;
use App\Domain\Property\Data\ValueObject\PropertyId;
use App\Domain\Property\Data\ValueObject\PropertyRef;
use App\Domain\Property\Data\ValueObject\Rent;
use App\Domain\Shared\Data\Constant\RoleConstant;
use App\Domain\Shared\Data\ValueObject\Address;
use App\Domain\Shared\Data\ValueObject\Country;
use App\Infrastructure\Controller\Admin\CustomerCrudController;
use App\Infrastructure\Database\Adapter\Property\Property;
use App\Infrastructure\Symfony\Form\Type\AddressType;
use App\Infrastructure\Symfony\Form\Type\PropertyRefType;
use App\Infrastructure\Symfony\Security\Data\CustomerSecurityAdapter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;

class PropertyCrudController extends AbstractCrudController
{

    public function __construct(private readonly TranslatorInterface $trans,
        private readonly AdminUrlGenerator $urlGenerator
    )
    {
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', $this->trans->trans('Property'))
            ->setPageTitle('new', $this->trans->trans('Add property'))
            ->setPageTitle('edit', $this->trans->trans('Update'))
            ->setPageTitle('detail', function (Property $property){
                return $property->ref;
            })
            ->setEntityLabelInSingular($this->trans->trans('Property'))
            ->setEntityLabelInPlural($this->trans->trans('Properties'))
            ->setFormThemes(['form/custom_types.html.twig', '@EasyAdmin/crud/form_theme.html.twig'])
            ->overrideTemplate('crud/new', 'crud/property/new.html.twig')
            ->overrideTemplate('crud/edit', 'crud/property/edit.html.twig')
            ->overrideTemplate('crud/detail', 'crud/property/detail.html.twig')
        ;
    }

    public static function getEntityFqcn(): string
    {
        return Property::class;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $currentUser = $this->getUser();
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);
        if( $currentUser instanceof CustomerSecurityAdapter){
            $queryBuilder->innerJoin('entity.propertyOwners', 'op')
                ->where('op.id = :ownerId')
                ->setParameter('ownerId', $currentUser->customer->id->getValue());
        }

        return $queryBuilder;
    }

    public function createEntity(string $entityFqcn): Property
    {
        $ref = '';
        $currentUser = $this->getUser();
        $propertyId = PropertyId::make();
        if( $currentUser instanceof CustomerSecurityAdapter)
        {
            $ref = strtoupper(str_split($currentUser->customer->name->getValue())[0] );
            $value = $propertyId->getValue();
            $ref .= date('ymd') . '-' . explode('-', $value)[0];
        }

        return new Property(
            $propertyId,
            new PropertyRef($ref),
            null,
            null,
            new Address(
                '',
                0,
                '',
                '',
                new Country('', ''),
                ''
            ),
            '',
            0,
            new Rent(500)
        );
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('ref', 'Ref')
                ->setFormType(PropertyRefType::class)
                ->setColumns('')
                ->formatValue(function (PropertyRef $ref, Property $property){
                    $url = $this->urlGenerator->setAction(Action::DETAIL)
                        ->setEntityId($property->id)
                        ->generateUrl();

                    return "<a href='$url'>$ref</a>";
                })
                ->hideOnDetail()
            ,
            AssociationField::new('affectedTypes', $this->trans->trans('Type'))
                ->setCssClass('type-label')
                ->formatValue(function (Collection $value){
                    $types = $value->map(function (PropertyType $type){
                        return $type->label;
                    })->toArray();

                    return implode(' | ', $types);
                })
                ->setColumns('')
            ,
            IntegerField::new('surface')
                ->formatValue(function ($value){
                    return $value . ' m2';
                })
                ->setColumns('')
            ,
            TextField::new('address', $this->trans->trans('Address'))
                ->setFormType(AddressType::class)
                ->setColumns('')
            ,
            MoneyField::new('rent.amount', $this->trans->trans('Rent Amount'))
                ->setCurrency('EUR')
                ->setColumns('')
            ,
            AssociationField::new('propertyOwners', $this->trans->trans('Owner'))
                ->onlyOnIndex()
                ->setPermission(RoleConstant::ADMIN_ROLE)
                ->formatValue(function (Collection $value){
                    $names = $value->map(function (Customer $customer){
                        $url = $this->urlGenerator
                            ->setController(CustomerCrudController::class)
                            ->setAction(Action::DETAIL)
                            ->setEntityId($customer->id)
                            ->generateUrl();
                        $name = $customer->name->getValue();

                        return "<a href='$url'>$name</a>";

                    })->toArray();

                    return implode(' | ', $names);
                })
                ->setColumns('')
        ];
    }
}
