<?php

namespace App\Infrastructure\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends AbstractController
{

    #[Route('/logout')]
    public function __invoke(Security $security): RedirectResponse
    {
        $security->logout();

        return $this->redirectToRoute('app_login');
    }
}