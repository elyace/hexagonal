<?php

namespace App\Infrastructure\Controller;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\AccountNotification\SendActivationCodeUseCase;
use App\Domain\Customer\Data\Event\CustomerActivationCodeEvent;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;
use App\Domain\Shared\Gateway\MessageBusInterface;
use App\Infrastructure\Symfony\Security\Data\CollaboratorSecurityAdapter;
use App\Infrastructure\Symfony\Security\Data\CustomerSecurityAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{

    #[Route('/login', name: 'app_login')]
    public function __invoke(AuthenticationUtils $authenticationUtils): Response
    {

        return $this->render('login.html.twig', [
            'controller_name' => 'LoginController',
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'last_username' => $authenticationUtils->getLastUsername()
        ]);
    }

    /**
     * A gateway controller to redirect user to the correct dashboard
     *
     * @return RedirectResponse
     */
    #[Route('/login-success', name: 'login-success')]
    public function postLogin(MessageBusInterface $messageBus): RedirectResponse
    {
        $currentUser = $this->getUser();
        if( null === $currentUser )
        {
            return $this->redirectToRoute('app_login');
        }

        if( $currentUser instanceof CustomerSecurityAdapter)
        {
            if( !$currentUser->customer->isActive )
            {
                $messageBus->dispatch(
                    new CustomerActivationCodeEvent(
                        new ActivationCode($currentUser->customer->id)
                    )
                );

                return $this->redirectToRoute('activate-email', [
                    'customer' => $currentUser->customer->id->getValue()
                ]);
            }

            if( $currentUser->customer->freeCustomer() )
            {
                return $this->redirectToRoute('start-enrollment');
            }

            return $this->redirectToRoute('customer-dashboard');
        }

        if( $currentUser instanceof CollaboratorSecurityAdapter && $currentUser->isAdmin()){
            return $this->redirectToRoute('admin-dashboard');
        }

        if( $currentUser instanceof CollaboratorSecurityAdapter){
            return $this->redirectToRoute('collaborator-dashboard');
        }

        return $this->redirectToRoute('home');
    }
}