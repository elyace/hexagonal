<?php

namespace App\Infrastructure\Controller\Customer;

use App\Domain\Enrollment\Data\Contract\Enrollment;
use App\Domain\Enrollment\StartCustomerEnrollmentUseCase;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;
use App\Infrastructure\Symfony\Form\Type\EnrollmentType;
use App\Infrastructure\Symfony\Security\Data\CustomerSecurityAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StartEnrollmentController extends AbstractController
{

    public function __construct(private readonly ValidatorInterface $validator, private readonly UseCaseFactoryInterface $useCaseFactory)
    {
    }

    #[Route('/{_locale}/start-enrollment', name: 'start-enrollment', requirements: ['_local' => 'en|fr'])]
    public function __invoke(Request $request): Response
    {
        /** @var CustomerSecurityAdapter $customer */
        $customer = $this->getUser();
        $form = $this->createForm(EnrollmentType::class, new Enrollment('', $customer->customer));
        $form->handleRequest($request);
        if( $form->isSubmitted() )
        {
            /** @var Enrollment $data */
            $data = $form->getData();
            $violations = $this->validator->validate($data);
            if( $violations->count() > 0 )
            {
                foreach ($violations as $violation) {
                    $form->addError(new FormError($violation->getMessage()));
                }
            }else{
                $this->useCaseFactory->setUseCase(StartCustomerEnrollmentUseCase::class)
                    ->execute($data);

                return $this->redirectToRoute('customer-dashboard');
            }
        }

        return $this->render('customer/start-enrollment.html.twig', [
            'form' => $form->createView()
        ]);
    }
}