<?php

namespace App\Infrastructure\Controller\Customer;

use App\Domain\Property\Data\Model\PropertyType;
use App\Domain\Shared\Data\Constant\RoleConstant;
use App\Infrastructure\Database\Adapter\Property\Property;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{

    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    /**
     * @return Response
     */
    #[Route('{_locale}/customer', name: 'customer-dashboard')]
    public function index(): Response
    {
        if ($this->isGranted(RoleConstant::PROPERTY_OWNER_ROLE))
        {
            return $this->render('customer/owner-dashboard.html.twig');
        }

        return $this->render('customer/tenant-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('PP Rent');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section($this->translator->trans('Property Management'))
            ->setPermission(RoleConstant::SOFT_PROPERTY_MANAGER_ROLE)
        ;
        yield MenuItem::linkToCrud($this->translator->trans('My Properties'), 'fas fa-building', Property::class)
            ->setPermission(RoleConstant::PROPERTY_OWNER_ROLE)
        ;
        yield MenuItem::linkToCrud($this->translator->trans('Property types'), 'fas fa-leaf', PropertyType::class)
            ->setPermission(RoleConstant::SOFT_PROPERTY_MANAGER_ROLE)
        ;
    }
}