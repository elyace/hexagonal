<?php

namespace App\Infrastructure\Controller\Customer;

use App\Domain\Customer\ActivateCustomerAccountUseCase;
use App\Domain\Customer\Data\Contract\CustomerActivate;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Exception\InvalidActivationCodeException;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;
use App\Infrastructure\Symfony\Form\Type\ActivateEmailType;
use App\Infrastructure\Symfony\Security\Data\CustomerSecurityAdapter;
use Doctrine\DBAL\Driver\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailVerifyController extends AbstractController
{

    private UseCaseFactoryInterface $useCaseFactory;

    public function __construct(UseCaseFactoryInterface $useCaseFactory)
    {
        $this->useCaseFactory = $useCaseFactory;
    }

    #[Route('/activate-email/{customer}', name: 'activate-email')]
    public function __invoke(Customer $customer, Request $request): Response
    {
        $currentUser = $this->getUser();
        if( $currentUser instanceof CustomerSecurityAdapter && $currentUser->customer->isActive)
        {
            return $this->redirectToRoute('login-success');
        }
        $form = $this->createForm(ActivateEmailType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            try {
                $this->useCaseFactory->setUseCase(ActivateCustomerAccountUseCase::class)
                    ->execute(
                        new CustomerActivate(
                            $customer->id,
                            $data['activationCode']
                        )
                    );
                return $this->redirectToRoute('login-success');
            }catch (InvalidActivationCodeException $e)
            {
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('customer/activate-email.html.twig', [
            'form' => $form->createView(),
            'email' => $customer->email,
            'resendLink' => $this->generateUrl(
                'resend-activation-code',
                ['customer' => $customer->id->getValue()]
            )
        ]);
    }
}