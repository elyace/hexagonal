<?php

namespace App\Infrastructure\Controller\Customer;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\Customer\Data\Event\CustomerActivationCodeEvent;
use App\Domain\Customer\Data\Model\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ResendActivationCodeController extends AbstractController
{

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    #[Route('/resend-code/{customer}', name: 'resend-activation-code')]
    public function __invoke(Customer $customer): RedirectResponse
    {
        $code = '';
        for ($i = 0; $i < 6; $i++) {
            $code .= mt_rand(0, 9);
        }

        $this->bus->dispatch(
            new CustomerActivationCodeEvent(
                new ActivationCode($code, $customer->id)
            )
        );

        return $this->redirectToRoute('activate-email', ['customer' => $customer->id->getValue()]);
    }
}