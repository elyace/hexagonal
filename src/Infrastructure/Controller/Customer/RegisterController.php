<?php

namespace App\Infrastructure\Controller\Customer;

use App\Domain\Customer\CustomerRegisterUseCase;
use App\Domain\Customer\Data\Contract\CustomerRegister;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Exception\EmailAlreadyTakenException;
use App\Domain\Shared\Exception\DomainException;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;
use App\Infrastructure\Symfony\Form\Data\CustomerRegisterData;
use App\Infrastructure\Symfony\Form\Type\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends AbstractController
{

    private UseCaseFactoryInterface $useCaseFactory;
    private ValidatorInterface $validator;

    public function __construct(UseCaseFactoryInterface $useCaseFactory, ValidatorInterface $validator)
    {
        $this->useCaseFactory = $useCaseFactory;
        $this->validator = $validator;
    }

    /**
     * @throws DomainException
     */
    #[Route('/register', name: 'register')]
    public function __invoke(Request $request): Response
    {

        $currentUser = $this->getUser();
        if( null !== $currentUser )
        {
            return $this->redirectToRoute('login-success');
        }

        $form = $this->createForm(RegisterType::class, new CustomerRegisterData());
        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid() )
        {
            /** @var CustomerRegisterData $data */
            $data = $form->getData();
            $register = new CustomerRegister(
                $data->name,
                $data->email,
                $data->password
            );

            $violations = $this->validator->validate($register);
            if( $violations->count() > 0 ){
                foreach ($violations as $violation) {
                    $form->get($violation->getPropertyPath())
                        ->addError( new FormError($violation->getMessage()) );
                }

                return $this->render('customer/register.html.twig', [
                    'form' => $form->createView()
                ]);
            }

            try {
                /** @var Customer $customer */
                $customer = $this->useCaseFactory->setUseCase(CustomerRegisterUseCase::class)
                    ->execute($register);

                return $this->redirectToRoute('activate-email', ['customer' => $customer->id->getValue()]);
            }catch (EmailAlreadyTakenException $exception)
            {
                $form->get('email')->addError(new FormError('Sorry this one has already been taken'));
            }
        }

        return $this->render('customer/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}