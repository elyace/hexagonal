<?php

namespace App\Infrastructure\Database\EventListener;

use App\Infrastructure\Database\Adapter\Property\Property;
use App\Infrastructure\Symfony\Security\Data\CustomerSecurityAdapter;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

#[AsEntityListener(event: Events::preFlush, method: '__invoke' , entity: Property::class)]
class PropertyPersistEventListener
{

    public function __construct(private readonly TokenStorageInterface $tokenStorage)
    {
    }

    /**
     * Set the property owner if not actually set yet
     *
     * @param Property $property
     * @return Property
     */
    public function __invoke(Property $property): Property
    {

        $currentUser = $this->tokenStorage->getToken()->getUser();
        if(
            $currentUser instanceof CustomerSecurityAdapter &&
            $currentUser->customer->isPropertyOwner() &&
            $property->propertyOwners === null
        )
        {
            $property->propertyOwners = new ArrayCollection([$currentUser->customer]);
        }

        return $property;
    }
}