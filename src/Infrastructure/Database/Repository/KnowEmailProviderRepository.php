<?php

namespace App\Infrastructure\Database\Repository;

use App\Domain\Customer\Repository\KnowEmailProviderRepositoryInterface;
use App\Domain\Shared\Data\ValueObject\Email;

class KnowEmailProviderRepository implements KnowEmailProviderRepositoryInterface
{

    public function __construct(private readonly array $unsafeEmails)
    {
    }

    public function isSafe(Email $email): bool
    {
        return !in_array($email->getProvider(), $this->unsafeEmails);
    }
}