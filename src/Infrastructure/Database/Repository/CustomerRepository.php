<?php

namespace App\Infrastructure\Database\Repository;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Shared\Data\ValueObject\Email;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method findOneByEmail(string $email)
 */
class CustomerRepository extends ServiceEntityRepository implements CustomerRepositoryInterface
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function customerExists(Email $email): bool
    {
        return $this->count(['email' => $email->getValue()]) > 0;
    }

    public function save(Customer $customer): void
    {
        $em = $this->getEntityManager();
        $em->persist($customer);
        $em->flush();
    }

    public function findOne(CustomerId $customerId): ?Customer
    {
        return $this->findOneBy(['id' => $customerId->getValue()]);
    }
}