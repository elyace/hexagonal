<?php

namespace App\Infrastructure\Database\Repository;

use App\Domain\Collaborator\Data\Model\Collaborator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CollaboratorRepository extends ServiceEntityRepository implements CollaboratorRepositoryInterface
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Collaborator::class);
    }
}