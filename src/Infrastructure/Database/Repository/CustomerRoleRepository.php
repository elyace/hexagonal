<?php

namespace App\Infrastructure\Database\Repository;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Enrollment\Repository\CustomerRoleRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CustomerRoleRepository extends ServiceEntityRepository implements CustomerRoleRepositoryInterface
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function updateRole(Customer $customer, string $role): void
    {
        $newRoles = [$role, ...$customer->roles->getValue()];
        $query = $this->createQueryBuilder('c')
            ->update(Customer::class, 'c')
            ->set('c.roles', ':roles')
            ->where('c.id=:id')
            ->setParameter('roles', json_encode(array_values($newRoles)))
            ->setParameter('id', $customer->id)
            ->getQuery();

        $query->execute();
    }
}