<?php

namespace App\Infrastructure\Database\Adapter\Property;

use App\Domain\Property\Data\Model\Property as PropertyModel;
use App\Domain\Property\Data\ValueObject\Owners;
use App\Domain\Property\Data\ValueObject\PropertyId;
use App\Domain\Property\Data\ValueObject\PropertyRef;
use App\Domain\Property\Data\ValueObject\PropertyTypes;
use App\Domain\Property\Data\ValueObject\Rent;
use App\Domain\Property\Data\ValueObject\RentCharge;
use App\Domain\Shared\Data\ValueObject\Address;
use Doctrine\Common\Collections\Collection;

class Property extends PropertyModel
{

    public function __construct(
        PropertyId $id,
        PropertyRef $ref,
        // Since domain model have not to know all about doctrine collection
        // we have to create an adapter to hidde it
        public ?Collection $affectedTypes,
        public ?Collection $propertyOwners,
        Address $address,
        string $description,
        float $surface,
        Rent $rent,
        bool $rentChargeIncluded = true,
        ?RentCharge $charge = null
    )
    {

        parent::__construct(
            $id,
            $ref,
            // then we transform it to domain value object
            new PropertyTypes($affectedTypes?->toArray() ?? []),
            new Owners( $propertyOwners?->toArray() ?? [] ),
            $address,
            $description,
            $surface,
            $rent,
            $rentChargeIncluded,
            $charge
        );
    }
}