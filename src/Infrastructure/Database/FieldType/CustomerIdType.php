<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Customer\Data\ValueObject\CustomerId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class CustomerIdType extends StringType
{

    const CUSTOMER_ID = 'customerId';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof CustomerId ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): CustomerId
    {
        return new CustomerId($value);
    }

    public function getName(): string
    {
        return self::CUSTOMER_ID;
    }
}