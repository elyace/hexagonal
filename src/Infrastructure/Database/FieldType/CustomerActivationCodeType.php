<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class CustomerActivationCodeType extends StringType
{
    const CUSTOMER_ACTIVATION_CODE = 'customerActivationCode';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof CustomerActivationCode ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): CustomerActivationCode
    {
        return new CustomerActivationCode($value);
    }

    public function getName(): string
    {
        return self::CUSTOMER_ACTIVATION_CODE;
    }
}