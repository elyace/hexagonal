<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Property\Data\ValueObject\PropertyId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class PropertyIdType extends StringType
{

    const PROPERTY_ID_TYPE = 'propertyIdType';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof PropertyId ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): PropertyId
    {
        return new PropertyId($value);
    }

    public function getName(): string
    {
        return self::PROPERTY_ID_TYPE;
    }
}