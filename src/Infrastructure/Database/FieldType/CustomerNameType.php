<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Customer\Data\ValueObject\CustomerName;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class CustomerNameType extends StringType
{

    const CUSTOMER_NAME = 'customerName';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof CustomerName ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): CustomerName
    {
        return new CustomerName($value);
    }

    public function getName(): string
    {
        return self::CUSTOMER_NAME;
    }
}