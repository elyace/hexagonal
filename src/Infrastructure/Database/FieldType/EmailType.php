<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Shared\Data\ValueObject\Email;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class EmailType extends StringType
{

    const EMAIL = 'email';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof Email ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Email
    {
        return new Email($value);
    }

    public function getName(): string
    {
        return self::EMAIL;
    }
}