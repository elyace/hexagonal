<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Property\Data\ValueObject\PropertyTypeId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class PropertyTypeIdType extends StringType
{

    const PROPERTY_TYPE_ID = 'propertyTypeId';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof PropertyTypeId ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): PropertyTypeId
    {
        return new PropertyTypeId($value);
    }

    public function getName(): string
    {
        return self::PROPERTY_TYPE_ID;
    }
}