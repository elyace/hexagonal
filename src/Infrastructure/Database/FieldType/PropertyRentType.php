<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Property\Data\ValueObject\Rent;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\FloatType;

final class PropertyRentType extends FloatType
{

    const PROPERTY_RENT_TYPE = 'propertyRentType';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof Rent ) return $value->amount;

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Rent
    {
        return new Rent($value);
    }

    public function getName(): string
    {
        return self::PROPERTY_RENT_TYPE;
    }
}