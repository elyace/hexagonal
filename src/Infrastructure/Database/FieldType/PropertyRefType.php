<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Property\Data\ValueObject\PropertyRef;
use App\Domain\Property\Data\ValueObject\Rent;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;

final class PropertyRefType extends TextType
{

    const PROPERTY_REF_TYPE = 'propertyRefType';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof PropertyRef ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): PropertyRef
    {
        return new PropertyRef($value);
    }

    public function getName(): string
    {
        return self::PROPERTY_REF_TYPE;
    }
}