<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Collaborator\Data\ValueObject\CollaboratorId;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class CollaboratorIdType extends StringType
{

    const COLLABORATOR_ID = 'collaboratorId';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof CollaboratorId ) return $value->getValue();

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): CollaboratorId
    {
        return new CollaboratorId($value);
    }

    public function getName(): string
    {
        return self::COLLABORATOR_ID;
    }
}