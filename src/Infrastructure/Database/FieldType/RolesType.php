<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Shared\Data\Constant\RoleConstant;
use App\Domain\Shared\Data\ValueObject\Roles;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\SimpleArrayType;

class RolesType extends SimpleArrayType
{

    const ROLES = 'roles';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof Roles) {
            $roles = $value->getValue();

            return json_encode(array_values($roles));
        }

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Roles
    {
        return new Roles(json_decode($value, true) ?? [RoleConstant::CUSTOMER_ROLE]);
    }

    public function getName(): string
    {
        return self::ROLES;
    }
}