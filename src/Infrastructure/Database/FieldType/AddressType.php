<?php

namespace App\Infrastructure\Database\FieldType;

use App\Domain\Shared\Data\ValueObject\Address;
use App\Domain\Shared\Data\ValueObject\Country;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;

class AddressType extends TextType
{

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value instanceof Address) {

            return json_encode($value);
        }

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Address
    {

        $data = json_decode($value, true);

        return new Address(
            $data['address'] ?? '',
            intval($data['streetNumber']),
            $data['street'],
            $data['town'] ?? '',
            new Country(
                $data['country']['name'],
                $data['country']['code']
            ),
            $data['postalCode'],
            $data['building'],
            $data['stairs'],
            $data['flourNumber']
        );
    }
}