<?php

namespace App\Infrastructure\Api\Resource;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Infrastructure\Api\Processor\CustomerEmailVerifyProcessor;

#[ApiResource(
    uriTemplate: 'email_verify',
    operations: [
        new Post(
            processor: CustomerEmailVerifyProcessor::class
        )
    ]
)]
final class EmailVerify
{
    public readonly string $code;
}