<?php

namespace App\Infrastructure\Api\Processor;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Domain\Customer\CustomerRegisterUseCase;
use App\Domain\Customer\Data\Contract\CustomerRegister;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;

class CustomerCreateProcessor implements ProcessorInterface
{

    public function __construct(private readonly UseCaseFactoryInterface $useCaseFactory)
    {
    }

    /**
     * @param CustomerRegister $data
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return void
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $this->useCaseFactory->setUseCase(CustomerRegisterUseCase::class)
            ->execute($data);
    }
}