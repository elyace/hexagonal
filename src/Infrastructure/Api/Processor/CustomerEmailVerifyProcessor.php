<?php

namespace App\Infrastructure\Api\Processor;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Domain\Customer\ActivateCustomerAccountUseCase;
use App\Domain\Customer\Data\Contract\CustomerActivate;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;

class CustomerEmailVerifyProcessor implements ProcessorInterface
{

    private UseCaseFactoryInterface $useCaseFactory;

    public function __construct(UseCaseFactoryInterface $useCaseFactory)
    {
        $this->useCaseFactory = $useCaseFactory;
    }

    /**
     * @param CustomerActivate $data
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return void
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $this->useCaseFactory->setUseCase(ActivateCustomerAccountUseCase::class)
            ->execute($data);
    }
}