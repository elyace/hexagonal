<?php

namespace App\Infrastructure\Api\Serializer;

use ApiPlatform\Metadata\Post;
use App\Domain\Customer\Data\Contract\CustomerRegister;
use App\Domain\Customer\Data\Model\Customer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class CustomerDenormalize implements DenormalizerInterface
{

    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): ?CustomerRegister
    {
        if( $context['operation']::class === Post::class )
        {
            return new CustomerRegister(
                $data['name'] ?? '',
                $data['email'] ?? '',
                ''
            );
        }

        return null;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return $type === Customer::class;
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            Customer::class => false
        ];
    }
}