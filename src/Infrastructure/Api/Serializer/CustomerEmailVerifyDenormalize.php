<?php

namespace App\Infrastructure\Api\Serializer;

use App\Domain\Customer\Data\Contract\CustomerActivate;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Infrastructure\Api\Resource\EmailVerify;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CustomerEmailVerifyDenormalize implements DenormalizerInterface
{

    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): CustomerActivate
    {
        return new CustomerActivate(
            new CustomerId( $data['customerId'] ?? '' ),
            $data['activationCode'] ?? ''
        );
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return EmailVerify::class === $type;
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            EmailVerify::class => false
        ];
    }
}