<?php

namespace App\Infrastructure\Api\Provider;

use ApiPlatform\Doctrine\Orm\Paginator;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Infrastructure\Api\Presenter\CustomerPresenter;

class CustomerCollectionPresenterProvider implements ProviderInterface
{

    public function __construct(private readonly ProviderInterface $collectionProvider)
    {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        /** @var Paginator $items */
        $items = $this->collectionProvider->provide($operation, $uriVariables, $context);
        $customers = [];
        foreach ($items as $item) {
            $customers[] = new CustomerPresenter(
                $item->id->getValue(),
                $item->name->getValue(),
                $item->email->getValue(),
                $item->isActive
            );
        }

        return $customers;
    }
}