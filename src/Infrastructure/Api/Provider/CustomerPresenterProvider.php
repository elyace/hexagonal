<?php

namespace App\Infrastructure\Api\Provider;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Domain\Customer\Data\Model\Customer;
use App\Infrastructure\Api\Presenter\CustomerPresenter;

final class CustomerPresenterProvider implements ProviderInterface
{

    public function __construct(private readonly ProviderInterface $itemProvider)
    {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): ?CustomerPresenter
    {
        /** @var Customer $item */
        $item = $this->itemProvider->provide($operation, $uriVariables, $context);
        if( null !== $item )
        {
            return new CustomerPresenter(
                $item->id->getValue(),
                $item->name->getValue(),
                $item->email->getValue(),
                $item->isActive
            );
        }

        return null;
    }
}