<?php

namespace App\Infrastructure\Api\Presenter;


final class CustomerPresenter
{

    public function __construct(
        public readonly string $id,
        public readonly string $name,
        public readonly string $email,
        public readonly bool $isActivated,
    )
    {
    }
}