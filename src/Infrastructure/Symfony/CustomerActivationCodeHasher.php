<?php

namespace App\Infrastructure\Symfony;

use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class CustomerActivationCodeHasher implements CustomerActivationCodeHasherInterface
{

    private PasswordHasherInterface $hasher;
    public function __construct(private readonly PasswordHasherFactoryInterface $hasherFactory)
    {
        $this->hasher = $this->hasherFactory->getPasswordHasher('customer');
    }

    public function isValid(CustomerActivationCode $code, string $plaintCode): bool
    {
        return $this->hasher->verify($code->getValue(), $plaintCode);
    }

    public function hashCode(string $plaintCode): CustomerActivationCode
    {
        return new CustomerActivationCode(
            $this->hasher->hash($plaintCode)
        );
    }
}