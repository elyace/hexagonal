<?php

namespace App\Infrastructure\Symfony;

use App\Domain\Shared\Gateway\TranslatorInterface as DomainTranslatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Translator implements DomainTranslatorInterface
{

    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function trans(string $text): string
    {
        return $this->translator->trans($text);
    }
}