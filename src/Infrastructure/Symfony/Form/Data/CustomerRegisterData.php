<?php

namespace App\Infrastructure\Symfony\Form\Data;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

class CustomerRegisterData implements PasswordAuthenticatedUserInterface
{
    public string $name;
    public string $email;
    public string $password;
    public string $rePassword;

    public function __construct(
        string $name = '',
        string $email = '',
        string $password = '',
        string $rePassword='',
    )
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->rePassword = $rePassword;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }
}