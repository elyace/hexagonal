<?php

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Shared\Data\ValueObject\Roles;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class RolesType extends CollectionType implements DataTransformerInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
        parent::buildForm($builder, $options);
    }

    /**
     * @param Roles $value
     * @return array
     */
    public function transform(mixed $value): array
    {
        return $value->getValue();
    }


    public function reverseTransform(mixed $value): Roles
    {
        return new Roles($value);
    }
}