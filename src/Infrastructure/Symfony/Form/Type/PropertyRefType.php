<?php

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Property\Data\ValueObject\PropertyRef;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PropertyRefType extends TextType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    /**
     * @param PropertyRef $data
     * @return string
     */
    public function transform(mixed $data): string
    {
        return $data->getValue();
    }

    public function reverseTransform(mixed $data): PropertyRef
    {
        return new PropertyRef($data);
    }
}