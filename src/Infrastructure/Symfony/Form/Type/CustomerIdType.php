<?php

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Customer\Data\ValueObject\CustomerId;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerIdType extends TextType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    public function reverseTransform(mixed $data): CustomerId
    {
        return new CustomerId($data);
    }
}