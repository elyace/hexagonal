<?php

namespace App\Infrastructure\Symfony\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegisterType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $trans)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => $this->trans->trans('Name'),
                'attr' => [
                    'placeholder' => 'Joe'
                ]
            ])
            ->add('email', \Symfony\Component\Form\Extension\Core\Type\EmailType::class, [
                'label' => 'Email',
                'attr' => [
                    'placeholder' => 'joe@pp-rent.com'
                ]
            ])
            ->add('plaintPassword', PasswordType::class, [
                'label' => $this->trans->trans('Your password'),
                'hash_property_path' => 'password',
                'mapped' => false,
                'attr' => [
                    'placeholder' => '******'
                ]
            ])
            ->add('rePassword', PasswordType::class, [
                'label' => $this->trans->trans('Confirm your password'),
                'attr' => [
                    'placeholder' => '******'
                ]
            ]);
    }
}