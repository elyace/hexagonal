<?php

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Shared\Data\ValueObject\Address;
use App\Domain\Shared\Data\ValueObject\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Countries;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddressType extends AbstractType implements DataTransformerInterface
{

    public function __construct(private readonly TranslatorInterface $trans)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addModelTransformer($this)
            ->add('address', TextType::class, [
                'label' => $this->trans->trans('Address')
            ])
            ->add('streetNumber', TextType::class, [
                'label' => $this->trans->trans('Street number')
            ])
            ->add('street', TextType::class, [
                'label' => $this->trans->trans('Street')
            ])
            ->add('town', TextType::class, [
                'label' => $this->trans->trans('Town')
            ])
            ->add('country', CountryType::class, [
                'label' => $this->trans->trans('Country')
            ])
            ->add('postalCode', TextType::class, [
                'label' => $this->trans->trans('Postal code')
            ])
            ->add('building', TextType::class, [
                'label' => $this->trans->trans('Building'),
                'required' => false,
            ])
            ->add('floorNumber', TextType::class, [
                'label' => $this->trans->trans('Floor Number'),
                'required' => false,
            ])
            ->add('stairs', TextType::class, [
                'label' => $this->trans->trans('Stairs'),
                'required' => false,
            ])
        ;
    }

    public function transform(mixed $value)
    {
        if( $value instanceof Address ) {
            $data = $value->jsonSerialize();

            return [
                ...$data,
                'country' => $data['country']['name']
            ];
        }

        return $value;
    }

    public function reverseTransform(mixed $value): Address
    {
        return new Address(
            $value['address'],
            $value['streetNumber'],
            $value['street'],
            $value['town'],
            new Country(
                Countries::getName($value['country']),
                $value['country']
            ),
            $value['postalCode'],
            $value['building'],
            $value['stairs'],
            $value['flourNumber'],
        );
    }
}