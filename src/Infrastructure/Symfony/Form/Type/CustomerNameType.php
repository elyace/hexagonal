<?php

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Customer\Data\ValueObject\CustomerName;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerNameType extends TextType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    public function reverseTransform(mixed $data): CustomerName
    {
        return new CustomerName($data);
    }
}