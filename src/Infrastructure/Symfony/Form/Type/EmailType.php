<?php

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Shared\Data\ValueObject\Email;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType as SymfonyEmailType;
use Symfony\Component\Form\FormBuilderInterface;

class EmailType extends SymfonyEmailType implements DataTransformerInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    public function reverseTransform(mixed $data): Email
    {
        return new Email($data);
    }

    /**
     * @param Email $value
     */
    public function transform(mixed $value)
    {
        return $value->getValue();
    }
}