<?php

namespace App\Infrastructure\Symfony\MessageHandler;

use App\Domain\AccountNotification\SendActivationCodeUseCase;
use App\Domain\Customer\Data\Event\CustomerActivationCodeEvent;
use App\Domain\Shared\Exception\DomainException;
use App\Domain\Shared\Factory\UseCaseFactoryInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class SendActivationCodeMessageHandler
{

    public function __construct(private readonly UseCaseFactoryInterface $useCaseFactory)
    {
    }

    /**
     * @throws DomainException
     */
    public function __invoke(CustomerActivationCodeEvent $event): void
    {
        $this->useCaseFactory->setUseCase(SendActivationCodeUseCase::class)
            ->execute($event->activationCode);
    }
}