<?php

namespace App\Infrastructure\Symfony;

use App\Domain\Shared\Gateway\MessageBusInterface as DomainMessageBusInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class MessageBusDispatcher implements DomainMessageBusInterface
{

    public function __construct( private readonly MessageBusInterface $bus)
    {
    }

    public function dispatch(object $object): void
    {
        $this->bus->dispatch($object);
    }
}