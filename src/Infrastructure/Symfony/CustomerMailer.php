<?php

namespace App\Infrastructure\Symfony;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Shared\Gateway\CustomerEmailSenderInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class CustomerMailer implements CustomerEmailSenderInterface
{

    public function __construct(private  readonly MailerInterface $mailer)
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function send(Customer $customer, string $subject, string $content): void
    {
        $email = (new TemplatedEmail())
            ->from('contact@locabien.io')
            ->to($customer->email->getValue())
            ->subject($subject)
            ->htmlTemplate('email/customer-email-wrapper.html.twig')
            ->context([
                'content' => $content
            ])
        ;
        $this->mailer->send($email);
    }
}