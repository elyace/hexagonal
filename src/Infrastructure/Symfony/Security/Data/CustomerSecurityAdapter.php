<?php

namespace App\Infrastructure\Symfony\Security\Data;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Shared\Data\Constant\RoleConstant;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CustomerSecurityAdapter implements UserInterface, PasswordAuthenticatedUserInterface
{

    public readonly Customer $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function getRoles(): array
    {
        return [
            RoleConstant::USER_ROLE,
            ...$this->customer->roles->getValue()
        ];
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->customer->email->getValue();
    }

    public function getPassword(): ?string
    {
        return $this->customer->password;
    }
}