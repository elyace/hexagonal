<?php

namespace App\Infrastructure\Symfony\Security\Data;

use App\Domain\Collaborator\Data\Model\Collaborator;
use App\Domain\Shared\Data\Constant\RoleConstant;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CollaboratorSecurityAdapter implements UserInterface, PasswordAuthenticatedUserInterface
{

    private Collaborator $collaborator;

    public function __construct(Collaborator $collaborator)
    {
        $this->collaborator = $collaborator;
    }

    public function getRoles(): array
    {
        return [
            RoleConstant::USER_ROLE,
            ...$this->collaborator->roles->getValue()
        ];
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->collaborator->email->getValue();
    }

    public function getPassword(): ?string
    {
        return $this->collaborator->password;
    }

    public function isAdmin(): bool
    {
        return in_array(RoleConstant::ADMIN_ROLE, $this->collaborator->roles->getValue());
    }
}