<?php

namespace App\Infrastructure\Symfony\Security;

use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Infrastructure\Symfony\Security\Data\CustomerSecurityAdapter;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CustomerProvider implements UserProviderInterface
{

    private CustomerRepositoryInterface $repository;

    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $this->loadUserByIdentifier($user->getUserIdentifier());
    }

    public function supportsClass(string $class): bool
    {
        return $class === CustomerSecurityAdapter::class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $customer = $this->repository->findOneBy(['email' => $identifier]);

        if( null === $customer )
        {
            throw new UserNotFoundException("User $identifier not found");
        }

        return new CustomerSecurityAdapter($customer);
    }
}