<?php

namespace App\Infrastructure\Symfony\Security;

use App\Infrastructure\Database\Repository\CollaboratorRepositoryInterface;
use App\Infrastructure\Symfony\Security\Data\CollaboratorSecurityAdapter;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CollaboratorProvider implements UserProviderInterface
{

    private CollaboratorRepositoryInterface $repository;

    public function __construct(CollaboratorRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    public function supportsClass(string $class): bool
    {
        return $class === CollaboratorSecurityAdapter::class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $collaborator = $this->repository->findOneBy(['email' => $identifier]);

        if( null === $collaborator )
        {
            throw new UserNotFoundException("User $identifier not found");
        }

        return new CollaboratorSecurityAdapter($collaborator);
    }
}