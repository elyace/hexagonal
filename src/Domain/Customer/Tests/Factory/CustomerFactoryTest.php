<?php

namespace App\Domain\Customer\Tests\Factory;


use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Customer\Factory\CustomerFactory;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Gateway\MessageBusInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class CustomerFactoryTest extends TestCase
{
    public function testCreateCustomer()
    {
        $hasher = $this->createMock(CustomerActivationCodeHasherInterface::class);
        $passwordHasher = $this->createMock(PasswordHasherInterface::class);
        $bus = $this->createMock(MessageBusInterface::class);
        $factory = new CustomerFactory($hasher, $bus, $passwordHasher);

        $customer = $factory->createCustomer(
            new CustomerName('John Doe'),
            new Email('john@gmail.com'),
            'some-text'
        );

        self::assertInstanceOf(Customer::class, $customer);
    }
}