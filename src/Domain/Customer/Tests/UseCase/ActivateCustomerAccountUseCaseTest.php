<?php

namespace App\Domain\Customer\Tests\UseCase;

use App\Domain\Customer\ActivateCustomerAccountUseCase;
use App\Domain\Customer\Data\Contract\CustomerActivate;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Customer\Exception\NotFoundException;
use App\Domain\Customer\Exception\InvalidActivationCodeException;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Gateway\MessageBusInterface;
use PHPUnit\Framework\TestCase;

class ActivateCustomerAccountUseCaseTest extends TestCase
{

    /**
     * @throws InvalidActivationCodeException
     */
    public function testThrowExceptionOnCustomerNotFound()
    {
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository->expects(self::once())
            ->method('findOne')
            ->willReturn(null)
        ;
        $hasher = $this->createMock(CustomerActivationCodeHasherInterface::class);
        $bus = $this->createMock(MessageBusInterface::class);
        $useCase = new ActivateCustomerAccountUseCase($repository, $hasher, $bus);
        self::expectException(NotFoundException::class);
        $useCase->__invoke( new CustomerActivate(CustomerId::make(), '123456') );
    }

    /**
     * @throws NotFoundException
     */
    public function testThrowExceptionOnInvalidActivationCode()
    {
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository->expects(self::once())
            ->method('findOne')
            ->willReturn(new Customer(
                CustomerId::make(),
                new CustomerName('John Doe'),
                new Email('john@gmail.com'),
                'some-text',
                false,
                new CustomerActivationCode('hashed-code')
            ))
        ;
        $hasher = $this->createMock(CustomerActivationCodeHasherInterface::class);
        $hasher->expects(self::once())
            ->method('isValid')
            ->willReturn(false)
        ;
        $bus = $this->createMock(MessageBusInterface::class);
        $useCase = new ActivateCustomerAccountUseCase($repository, $hasher, $bus);
        self::expectException(InvalidActivationCodeException::class);
        $useCase->__invoke( new CustomerActivate(CustomerId::make(), '123456') );
    }

    /**
     * @throws InvalidActivationCodeException
     * @throws NotFoundException
     */
    public function testCallingSave()
    {
        $activationCode = '123456';
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository->expects(self::once())
            ->method('findOne')
            ->willReturn(new Customer(
                CustomerId::make(),
                new CustomerName('John Doe'),
                new Email('john@gmail.com'),
                'some-text',
                false,
                new CustomerActivationCode('hashed-code')
            ))
        ;
        $hasher = $this->createMock(CustomerActivationCodeHasherInterface::class);
        $hasher->expects(self::once())
            ->method('isValid')
            ->willReturn(true)
        ;
        $bus = $this->createMock(MessageBusInterface::class);
        $bus->expects(self::once())
            ->method('dispatch')
        ;
        $useCase = new ActivateCustomerAccountUseCase($repository, $hasher, $bus);
        $repository->expects(self::once())
            ->method('save')
        ;
        $useCase->__invoke( new CustomerActivate(CustomerId::make(), $activationCode) );
    }
}