<?php

namespace App\Domain\Customer\Tests\UseCase;

use App\Domain\Customer\CustomerRegisterUseCase;
use App\Domain\Customer\Data\Contract\CustomerRegister;
use App\Domain\Customer\Exception\EmailAlreadyTakenException;
use App\Domain\Customer\Exception\UnsafeEmailException;
use App\Domain\Customer\Factory\CustomerFactoryInterface;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Customer\Repository\KnowEmailProviderRepositoryInterface;
use App\Domain\Shared\Data\ValueObject\Email;
use PHPUnit\Framework\TestCase;

class CustomerRegisterUseCaseTest extends TestCase
{

    /**
     * @throws UnsafeEmailException
     */
    public function testThrowExceptionOnCustomerExists()
    {
        $email = 'taken@email.com';
        $request = new CustomerRegister('John Doe', $email, 'some-text');
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository->expects(self::once())
            ->method('customerExists')
            ->with( new Email($email) )
            ->willReturn(true)
        ;
        $safeEmailRepository = $this->createMock(KnowEmailProviderRepositoryInterface::class);
        $factory = $this->createMock(CustomerFactoryInterface::class);
        $useCase = new CustomerRegisterUseCase(
            $repository,
            $safeEmailRepository,
            $factory
        );
        self::expectException(EmailAlreadyTakenException::class);
        $useCase->__invoke($request);
    }

    /**
     * @throws EmailAlreadyTakenException
     */
    public function testThrowExceptionOnUnsafeEmail()
    {
        $email = 'unsafe@email.com';
        $request = new CustomerRegister('John Doe', $email, 'some-text');
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $safeEmailRepository = $this->createMock(KnowEmailProviderRepositoryInterface::class);
        $safeEmailRepository->expects(self::once())
            ->method('isSafe')
            ->with( new Email($email) )
            ->willReturn(false)
        ;
        $factory = $this->createMock(CustomerFactoryInterface::class);
        $useCase = new CustomerRegisterUseCase(
            $repository,
            $safeEmailRepository,
            $factory
        );
        self::expectException(UnsafeEmailException::class);
        $useCase->__invoke($request);
    }

    /**
     * @throws UnsafeEmailException
     * @throws EmailAlreadyTakenException
     */
    public function testValidCustomerSaved()
    {
        $email = 'valid@email.com';
        $request = new CustomerRegister('John Doe', $email, 'some-text');
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository->expects(self::once())
            ->method('customerExists')
            ->with( new Email($email) )
            ->willReturn(false)
        ;
        $safeEmailRepository = $this->createMock(KnowEmailProviderRepositoryInterface::class);
        $safeEmailRepository->expects(self::once())
            ->method('isSafe')
            ->with( new Email($email) )
            ->willReturn(true)
        ;
        $factory = $this->createMock(CustomerFactoryInterface::class);
        $useCase = new CustomerRegisterUseCase(
            $repository,
            $safeEmailRepository,
            $factory
        );
        $repository->expects(self::once())
            ->method('save')
        ;
        $useCase->__invoke($request);
    }
}