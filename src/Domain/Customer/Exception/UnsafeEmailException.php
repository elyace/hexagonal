<?php

namespace App\Domain\Customer\Exception;

use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Exception\DomainException;

final class UnsafeEmailException extends DomainException
{
    public readonly Email $email;

    public function __construct(Email $email)
    {
        parent::__construct(sprintf('%s is not a valid safe email provider', $email->getProvider()));
        $this->email = $email;
    }
}