<?php

namespace App\Domain\Customer\Exception;

use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Shared\Exception\DomainException;

class NotFoundException extends DomainException
{
    public function __construct(CustomerId $customerId)
    {
        parent::__construct(sprintf("Customer with id %s was not found", $customerId->getValue()));
    }
}