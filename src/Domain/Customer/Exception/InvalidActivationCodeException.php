<?php

namespace App\Domain\Customer\Exception;

use App\Domain\Shared\Exception\DomainException;

class InvalidActivationCodeException extends DomainException
{

    /**
     * @param string $activationCode
     */
    public function __construct(string $activationCode)
    {
        parent::__construct(sprintf("Invalid activation code %s", $activationCode));
    }
}