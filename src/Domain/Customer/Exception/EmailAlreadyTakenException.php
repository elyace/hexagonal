<?php

namespace App\Domain\Customer\Exception;

use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Exception\DomainException;

final class EmailAlreadyTakenException extends DomainException
{
    public readonly Email $email;

    public function __construct(Email $email)
    {
        parent::__construct( sprintf('%s already taken', $email->getValue()) );
        $this->email = $email;
    }
}