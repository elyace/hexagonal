<?php

namespace App\Domain\Customer\Repository;

use App\Domain\Shared\Data\ValueObject\Email;

interface KnowEmailProviderRepositoryInterface
{
    public function isSafe(Email $email): bool;
}