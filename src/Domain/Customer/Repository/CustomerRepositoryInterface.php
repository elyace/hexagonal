<?php

namespace App\Domain\Customer\Repository;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Shared\Data\ValueObject\Email;

interface CustomerRepositoryInterface
{
    public function customerExists(Email $email): bool;

    public function save(Customer $customer): void;

    public function findOne(CustomerId $customerId): ?Customer;
}