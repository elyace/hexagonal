<?php

namespace App\Domain\Customer\Data\ValueObject;

use App\Domain\Shared\Data\ValueObject\Text;

final class CustomerName extends Text
{

    public function __construct(private readonly string $name)
    {
    }

    public function getValue(): string
    {
        return $this->name;
    }
}