<?php

namespace App\Domain\Customer\Data\ValueObject;

use App\Domain\Shared\Data\ValueObject\Text;

class CustomerActivationCode extends Text
{

    public function __construct(private readonly ?string $code)
    {
    }

    public function getValue(): string
    {
        return $this->code;
    }
}