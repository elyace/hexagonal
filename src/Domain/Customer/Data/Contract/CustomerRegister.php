<?php

namespace App\Domain\Customer\Data\Contract;

use Symfony\Component\Validator\Constraints as Assert;

class CustomerRegister
{

    #[Assert\NotBlank()]
    public readonly string $name;
    #[Assert\NotBlank()]
    #[Assert\Email()]
    public readonly string $email;
    #[Assert\NotBlank()]
    public readonly string $password;

    public function __construct(string $name, string $email, string $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }
}