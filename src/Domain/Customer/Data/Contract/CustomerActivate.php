<?php

namespace App\Domain\Customer\Data\Contract;

use App\Domain\Customer\Data\ValueObject\CustomerId;
use Symfony\Component\Validator\Constraints as Assert;

final class CustomerActivate
{

    #[Assert\Valid()]
    public readonly CustomerId $customerId;
    #[Assert\NotBlank()]
    public readonly string $activationCode;

    public function __construct(CustomerId $customerId, string $activationCode)
    {
        $this->customerId = $customerId;
        $this->activationCode = $activationCode;
    }
}