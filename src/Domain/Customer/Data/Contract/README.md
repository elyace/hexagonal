# CONTRACT

Contracts are DTO classes that rule use case input signature, most time, contract contain also data validations . 

Notice: Explicit validation does not occur at Domain level this is Infrastructure business, so we just define rules here.  
