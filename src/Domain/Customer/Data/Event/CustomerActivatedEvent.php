<?php

namespace App\Domain\Customer\Data\Event;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Shared\Data\Contract\AsyncEventInterface;

final class CustomerActivatedEvent implements AsyncEventInterface
{
    public function __construct(public readonly Customer $customer)
    {
    }
}