<?php

namespace App\Domain\Customer\Data\Event;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\Shared\Data\Contract\AsyncEventInterface;

final class CustomerActivationCodeEvent implements AsyncEventInterface
{
    public function __construct(public readonly ActivationCode $activationCode)
    {
    }
}