<?php

namespace App\Domain\Customer\Data\Model;

use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Shared\Data\Constant\RoleConstant;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Data\ValueObject\Roles;

class Customer
{

    public function __construct(
        public CustomerId $id,
        public CustomerName $name,
        public Email $email,
        public string $password,
        public Roles $roles,
        public bool $isActive = false,
        public ?CustomerActivationCode $activationCode = null
    )
    {
    }

    public function activateAccount(): self
    {
        $this->isActive = true;

        return $this;
    }

    /**
     * Did not start property owner or tenant enrollment yet
     *
     * @return bool
     */
    public function freeCustomer(): bool
    {
        return count($this->roles->getValue()) === 1 && $this->roles->hasRole(RoleConstant::CUSTOMER_ROLE);
    }

    public function getPrincipalRole(): string
    {
        $roles = array_values(array_filter($this->roles->getValue(), function (string $role){

            return $role !== RoleConstant::USER_ROLE && $role !== RoleConstant::CUSTOMER_ROLE;
        }));

        return $roles[0] ?? RoleConstant::CUSTOMER_ROLE;
    }

    public function isPropertyOwner(): bool
    {
        return $this->getPrincipalRole() === RoleConstant::PROPERTY_OWNER_ROLE;
    }
}