<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;

interface CustomerActivationCodeHasherInterface
{
    public function isValid(CustomerActivationCode $code, string $plaintCode): bool;

    public function hashCode(string $plaintCode): CustomerActivationCode;
}