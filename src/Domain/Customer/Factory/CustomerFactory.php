<?php

namespace App\Domain\Customer\Factory;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\Customer\Data\Event\CustomerActivationCodeEvent;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use App\Domain\Shared\Data\Constant\RoleConstant;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Data\ValueObject\Roles;
use App\Domain\Shared\Gateway\MessageBusInterface;

class CustomerFactory implements CustomerFactoryInterface
{

    public function __construct(
        private readonly MessageBusInterface $dispatcher
    )
    {
    }

    public function createCustomer(CustomerName $name, Email $email, ?string $password): Customer
    {
        $customerId = CustomerId::make();
        $customer = new Customer(
            $customerId,
            $name,
            $email,
            $password,
            new Roles([RoleConstant::CUSTOMER_ROLE]),
        );

        $this->dispatcher->dispatch(
            new CustomerActivationCodeEvent(
                new ActivationCode($customerId)
            )
        );

        return $customer;
    }
}