<?php

namespace App\Domain\Customer\Factory;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Shared\Data\ValueObject\Email;

interface CustomerFactoryInterface
{
    public function createCustomer(CustomerName $name, Email $email, ?string $password): Customer;
}