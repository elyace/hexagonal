<?php

namespace App\Domain\Customer;

use App\Domain\Customer\Data\Contract\CustomerActivate;
use App\Domain\Customer\Data\Event\CustomerActivatedEvent;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Exception\NotFoundException;
use App\Domain\Customer\Exception\InvalidActivationCodeException;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use App\Domain\Shared\Gateway\MessageBusInterface;

class ActivateCustomerAccountUseCase
{

    public function __construct(
        private readonly CustomerRepositoryInterface $repository,
        private readonly CustomerActivationCodeHasherInterface $hasher,
        private readonly MessageBusInterface $bus
    )
    {
    }

    /**
     * @param CustomerActivate $activate
     * @throws NotFoundException
     * @throws InvalidActivationCodeException
     */
    public function __invoke(CustomerActivate $activate): void
    {
        /** @var ?Customer $customer */
        $customer = $this->repository->findOne($activate->customerId);
        if( null === $customer )
        {
            throw new NotFoundException($activate->customerId);
        }
        if( ! $this->hasher->isValid($customer->activationCode, $activate->activationCode) )
        {
            throw new InvalidActivationCodeException($activate->activationCode);
        }
        $customer->activateAccount();
        $this->repository->save($customer);
        $this->bus->dispatch( new CustomerActivatedEvent($customer) );
    }
}