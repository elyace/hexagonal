<?php

namespace App\Domain\Customer;

use App\Domain\Customer\Data\Contract\CustomerRegister;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerName;
use App\Domain\Customer\Exception\EmailAlreadyTakenException;
use App\Domain\Customer\Exception\UnsafeEmailException;
use App\Domain\Customer\Factory\CustomerFactoryInterface;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Customer\Repository\KnowEmailProviderRepositoryInterface;
use App\Domain\Shared\Data\ValueObject\Email;

/**
 * Spec
 *
 * Registration require customer name and his email -
 * Email must be uniq per customer -
 * CustomerRegister email must be verified from known safe provider
 * CustomerRegister verify his email by 6 code digit sent in his inbox
 * The app do not have to know the verification code value
 */
final class CustomerRegisterUseCase
{

    public function __construct(
        private readonly CustomerRepositoryInterface $repository,
        private readonly KnowEmailProviderRepositoryInterface $emailProviderRepository,
        private readonly CustomerFactoryInterface $customerFactory
    )
    {
    }

    /**
     * @throws UnsafeEmailException
     * @throws EmailAlreadyTakenException
     */
    public function __invoke(CustomerRegister $register): Customer
    {
        $email = new Email($register->email);
        if( $this->repository->customerExists($email) )
        {
            throw new EmailAlreadyTakenException($email);
        }

        if( !$this->emailProviderRepository->isSafe($email) )
        {
            throw new UnsafeEmailException($email);
        }

        $name = new CustomerName($register->name);
        $customer = $this->customerFactory->createCustomer($name, $email, $register->password);
        $this->repository->save($customer);

        return $customer;
    }
}