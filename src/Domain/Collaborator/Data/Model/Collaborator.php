<?php

namespace App\Domain\Collaborator\Data\Model;

use App\Domain\Collaborator\Data\ValueObject\CollaboratorId;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Domain\Shared\Data\ValueObject\Roles;

class Collaborator
{

    public function __construct(
        public CollaboratorId $id,
        public string $name,
        public Email $email,
        public Roles $roles,
        public string $password
    )
    {
    }
}