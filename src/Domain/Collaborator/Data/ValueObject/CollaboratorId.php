<?php

namespace App\Domain\Collaborator\Data\ValueObject;

use App\Domain\Shared\Data\ValueObject\Text;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

final class CollaboratorId extends Text
{

    #[Assert\NotBlank()]
    private readonly string $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public static function make(): CollaboratorId
    {
        return new CollaboratorId(Uuid::uuid4());
    }

    public function getValue(): string
    {
        return $this->uuid;
    }
}