<?php

namespace App\Domain\Enrollment\Data\Contract;

use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Shared\Data\Constant\RoleConstant;
use Symfony\Component\Validator\Constraints as Assert;

class Enrollment
{

    #[Assert\NotBlank()]
    #[Assert\Choice(choices: ['tenant', 'owner'])]
    public string $role;
    public readonly Customer $customer;

    public function __construct(string $role, Customer $customer)
    {
        $this->role = $role;
        $this->customer = $customer;
    }

    public function getDomainRole(): string
    {
        switch ($this->role)
        {
            default:
            case 'tenant':
                return RoleConstant::TENANT_ROLE;
            case 'owner':
                return RoleConstant::PROPERTY_OWNER_ROLE;
        }
    }
}