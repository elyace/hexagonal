<?php

namespace App\Domain\Enrollment\Repository;

use App\Domain\Customer\Data\Model\Customer;

interface CustomerRoleRepositoryInterface
{
    public function updateRole(Customer $customer, string $role): void;
}