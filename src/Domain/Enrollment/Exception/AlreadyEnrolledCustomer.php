<?php

namespace App\Domain\Enrollment\Exception;

use App\Domain\Shared\Exception\DomainException;

class AlreadyEnrolledCustomer extends DomainException
{

    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}