<?php

namespace App\Domain\Enrollment;

use App\Domain\Enrollment\Data\Contract\Enrollment;
use App\Domain\Enrollment\Exception\AlreadyEnrolledCustomer;
use App\Domain\Enrollment\Repository\CustomerRoleRepositoryInterface;

class StartCustomerEnrollmentUseCase
{

    public function __construct(private readonly CustomerRoleRepositoryInterface $repository)
    {
    }

    /**
     * Initialise customer identification role after registration
     *
     * @param Enrollment $role
     * @return void
     * @throws AlreadyEnrolledCustomer
     */
    public function __invoke(Enrollment $role): void
    {

        if( !$role->customer->freeCustomer() )
        {
            throw new AlreadyEnrolledCustomer("Customer has been already enrolled");
        }

        $this->repository->updateRole($role->customer, $role->getDomainRole());
    }
}