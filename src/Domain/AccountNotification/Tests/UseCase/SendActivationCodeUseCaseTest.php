<?php

namespace App\Domain\AccountNotification\Tests\UseCase;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\AccountNotification\SendActivationCodeUseCase;
use App\Domain\Customer\Data\ValueObject\CustomerId;
use App\Domain\Customer\Exception\NotFoundException;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Shared\Gateway\CustomerEmailSenderInterface;
use PHPUnit\Framework\TestCase;

class SendActivationCodeUseCaseTest extends TestCase
{
    public function testThrowNotFoundCustomer()
    {
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $repository->expects(self::once())
            ->method('findOne')
            ->willReturn(null)
        ;
        $emailSender = $this->createMock(CustomerEmailSenderInterface::class);
        $useCase = new SendActivationCodeUseCase($emailSender, $repository);
        $activationCode = new ActivationCode('123456', CustomerId::make());
        self::expectException(NotFoundException::class);
        $useCase->__invoke($activationCode);
    }

    /**
     * @throws NotFoundException
     */
    public function testSuccessfulEmailSend()
    {
        $repository = $this->createMock(CustomerRepositoryInterface::class);
        $emailSender = $this->createMock(CustomerEmailSenderInterface::class);
        $useCase = new SendActivationCodeUseCase($emailSender, $repository);
        $activationCode = new ActivationCode('123456', CustomerId::make());
        $emailSender->expects(self::once())
            ->method('send')
        ;
        $useCase->__invoke($activationCode);
    }
}