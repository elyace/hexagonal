<?php

namespace App\Domain\AccountNotification;

use App\Domain\AccountNotification\Data\Contract\ActivationCode;
use App\Domain\Customer\Data\Model\Customer;
use App\Domain\Customer\Data\ValueObject\CustomerActivationCode;
use App\Domain\Customer\Exception\NotFoundException;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Domain\Customer\Service\CustomerActivationCodeHasherInterface;
use App\Domain\Shared\Gateway\CustomerEmailSenderInterface;
use App\Domain\Shared\Gateway\TranslatorInterface;

final class SendActivationCodeUseCase
{

    public function __construct(
        private readonly CustomerEmailSenderInterface $emailSender,
        private readonly CustomerRepositoryInterface $repository,
        private readonly TranslatorInterface $translator,
        private readonly CustomerActivationCodeHasherInterface $hasher
    )
    {
    }

    /**
     * @throws NotFoundException
     */
    public function __invoke(ActivationCode $activationCode): void
    {
        /** @var ?Customer $customer */
        $customer = $this->repository->findOne($activationCode->customerId) ;
        if( null === $customer )
        {
            throw new NotFoundException($activationCode->customerId);
        }
        $plainCode = $activationCode->generateCode();
        $customer->activationCode = new CustomerActivationCode($this->hasher->hashCode($plainCode));
        $this->repository->save($customer);

        $this->emailSender->send(
            $customer,
            $this->translator->trans("Activate your account"),
            $this->translator->trans(
                sprintf("Hi, use the code below to activate your account: %s", $plainCode)
            )
        );
    }
}