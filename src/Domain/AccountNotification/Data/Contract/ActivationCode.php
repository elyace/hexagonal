<?php

namespace App\Domain\AccountNotification\Data\Contract;

use App\Domain\Customer\Data\ValueObject\CustomerId;
use Symfony\Component\Validator\Constraints as Assert;

final class ActivationCode
{
    #[Assert\Valid()]
    public readonly CustomerId $customerId;

    public function __construct(CustomerId $customerId)
    {
        $this->customerId = $customerId;
    }

    public function generateCode(): int
    {
        $code = '';
        for ($i = 0; $i < 6; $i++) {
            $code .= mt_rand(0, 9);
        }

        return intval($code);
    }
}