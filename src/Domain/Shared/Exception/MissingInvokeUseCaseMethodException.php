<?php

namespace App\Domain\Shared\Exception;

class MissingInvokeUseCaseMethodException extends \Exception
{
    public function __construct(string $useCaseClass)
    {
        parent::__construct(sprintf("%s must implement __invoke method as use case", $useCaseClass));
    }
}