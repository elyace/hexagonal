<?php

namespace App\Domain\Shared\Data\Constant;

final class RoleConstant
{

    // default role for all authenticated user
    public const USER_ROLE = 'ROLE_USER';

    public const CUSTOMER_ROLE = 'ROLE_CUSTOMER';
    public const ADMIN_ROLE = 'ROLE_ADMIN';
    public const TENANT_ROLE = 'ROLE_TENANT';
    public const PROPERTY_OWNER_ROLE = 'ROLE_OWNER';

    // granted to manage some property features
    public const SOFT_PROPERTY_MANAGER_ROLE = 'ROLE_SOFT_PROPERTY_MANAGER';
}