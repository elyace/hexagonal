<?php

namespace App\Domain\Shared\Data\ValueObject;

class Address extends Text implements \JsonSerializable
{

    public function __construct(
        public readonly string $address,
        public readonly int $streetNumber,
        public readonly string $street,
        public readonly string $town,
        public readonly Country $country,
        public readonly string  $postalCode,
        public readonly ?string $building = null,
        public readonly ?string $stairs = null,
        public readonly ?string $flourNumber = null,
    )
    {
    }

    public function getValue(): string
    {
        $address = sprintf(
            '%s %s Rue %s %s, %s %s',
            $this->address,
            $this->streetNumber,
            $this->street,
            $this->town,
            $this->postalCode,
            $this->country->getValue()
        );

        $building = $this->building ? " Batiment " . $this->building .' ' : '';
        $stairs = $this->stairs ? " Escalier " . $this->stairs . ' ' : '';
        $flourNumber = $this->flourNumber ? " Etage " . $this->flourNumber . ' ' : '';

        return $address. $building . $stairs . $flourNumber;
    }

    public function jsonSerialize(): array
    {
        return [
            'address' => $this->address,
            'streetNumber' => $this->streetNumber,
            'street' => $this->street,
            'town' => $this->town,
            'country' => [
                'name' => $this->country->name,
                'code' => $this->country->code
            ],
            'postalCode' => $this->postalCode,
            'building' => $this->building,
            'stairs' => $this->stairs,
            'flourNumber' => $this->flourNumber,
        ];
    }
}