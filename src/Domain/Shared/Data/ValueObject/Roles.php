<?php

namespace App\Domain\Shared\Data\ValueObject;

use Traversable;

class Roles implements \ArrayAccess, \IteratorAggregate
{
    public function __construct(private array $roles)
    {
    }

    public function getValue(): array
    {
        return $this->roles;
    }

    public function offsetExists(mixed $offset): bool
    {
        return array_key_exists($offset, $this->roles);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->roles[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->roles[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->roles[$offset]);
    }

    public function getIterator(): Traversable
    {
        return (function () {
            foreach ($this->roles as $role) {
                yield $role;
            }
        })();
    }

    public function hasRole(string $role): bool
    {
        return in_array($role, $this->roles);
    }
}