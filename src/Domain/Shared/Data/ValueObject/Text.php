<?php

namespace App\Domain\Shared\Data\ValueObject;

abstract class Text implements \Stringable
{
    abstract public function getValue(): string;

    public function __toString(): string
    {
        return $this->getValue();
    }
}