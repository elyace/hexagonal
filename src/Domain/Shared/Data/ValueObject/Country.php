<?php

namespace App\Domain\Shared\Data\ValueObject;

class Country extends Text
{
    public function __construct(
        public readonly string $name,
        public readonly string $code
    )
    {
    }

    public function getValue(): string
    {
        return $this->name;
    }
}