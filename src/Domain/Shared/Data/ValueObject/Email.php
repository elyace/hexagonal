<?php

namespace App\Domain\Shared\Data\ValueObject;

final class Email extends Text
{

    private const SEPARATOR = '@';

    public function __construct(private readonly string $email)
    {
    }

    public function getValue(): string
    {
        return $this->email;
    }

    public function getProvider(): string
    {
        $parts = explode(self::SEPARATOR, $this->email);
        if( count($parts) === 2 )
        {
            return $parts[1];
        }

        return '';
    }

    public function getId(): string
    {
        $parts = explode(self::SEPARATOR, $this->email);
        if( count($parts) === 2 )
        {
            return $parts[0];
        }

        return '';
    }

    public function __toString(): string
    {
        $em   = explode("@", $this->email);
        $name = implode('@', array_slice($em, 0, count($em)-1));
        $len  = floor(strlen($name)/2);

        return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
    }
}