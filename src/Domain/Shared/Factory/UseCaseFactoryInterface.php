<?php

namespace App\Domain\Shared\Factory;

use App\Domain\Shared\Exception\DomainException;

interface UseCaseFactoryInterface
{
    public function setUseCase(string $useCaseClass): self;

    /**
     * @param object $request
     * @return mixed
     * @throws DomainException
     */
    public function execute(object $request): mixed;
}