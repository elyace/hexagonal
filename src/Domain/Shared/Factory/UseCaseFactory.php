<?php

namespace App\Domain\Shared\Factory;

use App\Domain\Shared\Exception\MissingInvokeUseCaseMethodException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class UseCaseFactory implements UseCaseFactoryInterface
{

    private ?object $useCase;
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $useCaseClass
     * @return UseCaseFactoryInterface
     * @throws MissingInvokeUseCaseMethodException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function setUseCase(string $useCaseClass): UseCaseFactoryInterface
    {
        $useCase = $this->container->get($useCaseClass);

        if( $useCase && !method_exists($useCase, '__invoke') )
        {
            throw new MissingInvokeUseCaseMethodException($useCaseClass);
        }

        $this->useCase = $useCase;

        return $this;
    }

    public function execute(object $request): mixed
    {
        return $this->useCase?->__invoke($request);
    }
}