<?php

namespace App\Domain\Shared\Gateway;

interface MessageBusInterface
{
    public function dispatch(object $object): void;
}