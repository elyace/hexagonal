<?php

namespace App\Domain\Shared\Gateway;

interface TranslatorInterface
{
    public function trans(string $text): string;
}