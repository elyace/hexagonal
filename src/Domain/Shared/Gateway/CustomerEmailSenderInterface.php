<?php

namespace App\Domain\Shared\Gateway;

use App\Domain\Customer\Data\Model\Customer;

interface CustomerEmailSenderInterface
{

    public function send(Customer $customer, string $subject, string $content): void;
}