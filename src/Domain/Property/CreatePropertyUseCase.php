<?php

namespace App\Domain\Property;

use App\Domain\Property\Data\Contract\PropertyCreate;

/**
 * Spec
 *
 * Reference must be uniq and required
 * Initial rent is required and can be change when property being in rent
 */
final class CreatePropertyUseCase
{

    public function __invoke(PropertyCreate $propertyCreate): void
    {
        // TODO: Implement __invoke() method.
    }
}