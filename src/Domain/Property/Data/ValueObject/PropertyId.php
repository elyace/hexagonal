<?php

namespace App\Domain\Property\Data\ValueObject;

use App\Domain\Shared\Data\ValueObject\Text;
use Ramsey\Uuid\Uuid;

class PropertyId extends Text
{

    public function __construct(private readonly string $propertyId)
    {
    }

    public function getValue(): string
    {
        return $this->propertyId;
    }

    public static function make(): PropertyId
    {
        return new PropertyId(Uuid::uuid4());
    }

}