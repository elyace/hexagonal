<?php

namespace App\Domain\Property\Data\ValueObject;

class Owners
{

    public function __construct(public array $owners)
    {
    }
}