<?php

namespace App\Domain\Property\Data\ValueObject;

final class RentCharge
{

    public function __construct(private readonly float $amount)
    {
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}