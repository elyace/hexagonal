<?php

namespace App\Domain\Property\Data\ValueObject;

class PropertyTypes
{

    public function __construct(public array $types)
    {
    }
}