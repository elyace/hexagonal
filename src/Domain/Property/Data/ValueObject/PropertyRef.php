<?php

namespace App\Domain\Property\Data\ValueObject;

use App\Domain\Shared\Data\ValueObject\Text;

class PropertyRef extends Text
{

    public function __construct(public readonly string $reference)
    {
    }

    public function getValue(): string
    {
        return $this->reference;
    }
}