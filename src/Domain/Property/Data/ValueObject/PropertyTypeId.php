<?php

namespace App\Domain\Property\Data\ValueObject;

use App\Domain\Shared\Data\ValueObject\Text;
use Ramsey\Uuid\Uuid;

class PropertyTypeId extends Text
{

    public function __construct(private readonly string $id)
    {
    }

    public function getValue(): string
    {
        return $this->id;
    }

    public static function make(): PropertyTypeId
    {
        return new PropertyTypeId(Uuid::uuid4());
    }
}