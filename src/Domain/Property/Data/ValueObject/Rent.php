<?php

namespace App\Domain\Property\Data\ValueObject;

final class Rent
{
    public function __construct(public float $amount, public ?string $device = null)
    {
    }
}