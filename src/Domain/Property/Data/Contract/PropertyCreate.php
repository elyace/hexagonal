<?php

namespace App\Domain\Property\Data\Contract;

use Symfony\Component\Validator\Constraints as Assert;

final class PropertyCreate
{

    #[Assert\NotBlank()]
    public string $reference;
    #[Assert\NotBlank()]
    public string $description;
    #[Assert\NotBlank()]
    public float $surface;
    #[Assert\NotBlank()]
    public float $rentAmount;
    public bool $includedCharge;
    public ?float $chargeAmount;

    public function __construct(
        string $reference,
        string $description,
        float $surface,
        float $rentAmount,
        bool $includedCharge,
        ?float $chargeAmount = null
    )
    {
        $this->reference = $reference;
        $this->description = $description;
        $this->surface = $surface;
        $this->rentAmount = $rentAmount;
        $this->includedCharge = $includedCharge;
        $this->chargeAmount = $chargeAmount;
    }
}