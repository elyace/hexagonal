<?php

namespace App\Domain\Property\Data\Model;

use App\Domain\Property\Data\ValueObject\PropertyTypeId;

final class PropertyType implements \Stringable
{

    public function __construct(
        public PropertyTypeId $id,
        public string $label
    )
    {
    }

    public function __toString()
    {
        return $this->label;
    }
}