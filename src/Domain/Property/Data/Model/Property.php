<?php

namespace App\Domain\Property\Data\Model;

use App\Domain\Property\Data\ValueObject\Owners;
use App\Domain\Property\Data\ValueObject\PropertyId;
use App\Domain\Property\Data\ValueObject\PropertyRef;
use App\Domain\Property\Data\ValueObject\PropertyTypes;
use App\Domain\Property\Data\ValueObject\Rent;
use App\Domain\Property\Data\ValueObject\RentCharge;
use App\Domain\Shared\Data\ValueObject\Address;

class Property
{

    public function __construct(
        public PropertyId    $id,
        public PropertyRef   $ref,
        public PropertyTypes $types,
        public Owners        $owner,
        public Address       $address,
        public string        $description,
        public float         $surface,
        public Rent          $rent,
        public bool          $rentChargeIncluded = true,
        public ?RentCharge   $charge = null,
    )
    {
    }
}