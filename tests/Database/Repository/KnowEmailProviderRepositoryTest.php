<?php

namespace App\Tests\Database\Repository;

use App\Domain\Customer\Repository\KnowEmailProviderRepositoryInterface;
use App\Domain\Shared\Data\ValueObject\Email;
use App\Infrastructure\Database\Repository\KnowEmailProviderRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class KnowEmailProviderRepositoryTest extends KernelTestCase
{

    /**
     * @throws \Exception
     */
    public function testGettingUnsafeEmails()
    {
        /** @var KnowEmailProviderRepositoryInterface $repository */
        $repository = $this->getContainer()->get(KnowEmailProviderRepository::class);
        $isSafe = $repository->isSafe(new Email('rakoto@gmail.com'));
        self::assertTrue($isSafe);
        $isSafe = $repository->isSafe(new Email('rakoto@yopmail.com'));
        self::assertFalse($isSafe);
    }
}
